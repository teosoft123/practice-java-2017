package timeconversion;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Scanner;

public class Solution {

	static {
		String input = "07:05:45PM";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	static String timeConversion(String s) {
		DateTimeFormatter dateInf = DateTimeFormatter.ofPattern("hh:mm:ssa");
		DateTimeFormatter dateOutf = DateTimeFormatter.ofPattern("HH:mm:ss");
		TemporalAccessor parsedDate = dateInf.parse(s);
		return dateOutf.format(parsedDate);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			String s = in.next();
			String result = timeConversion(s);
			System.out.println(result);
		}
	}
}
