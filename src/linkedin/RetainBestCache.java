package linkedin;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class DataSource<K, T> {

    public T get(K key) {
        return null;
    }
}

class Rankable {

}

class SortedList {

    public <K, T extends Rankable> Pair<K, T> getLowest() {
        return null;
    }

    public void add(Pair pair) {

    }
}

public class RetainBestCache<K, T extends Rankable> {

    private final DataSource<K, T> ds;
    private int maxEntries = 10;
    Map<K, T> cache = new ConcurrentHashMap<>();
    SortedList entryList = null; //... // sorted on insert by T only
    // having getLowest() operation


    /* Constructor with a data source (assumed to be slow) and a cache size */
    public RetainBestCache(DataSource<K, T> ds, int entriesToRetain) {
        this.ds = ds;
        this.maxEntries = entriesToRetain;
        // Implementation here
    }

    /* Gets some data. If possible, retrieves it from cache to be fast. If the data is not cached,
     * retrieves it from the data source. If the cache is full, attempt to cache the returned data,
     * evicting the T with lowest rank among the ones that it has available
     * If there is a tie, the cache may choose any T with lowest rank to evict.
     */
    public T get(K key) {
        T entry = cache.get(key); // assuming ConcurrentHashMap doesnt allow null elements
        if (null != entry) {
            return entry;
        }
        entry = ds.get(key); // returns null if not founds
        if (null != entry) {
            // put and maybe evict
            put(key, entry);
            return entry;

        }
        return null;
        // Implementation here
    }

    public void put(K key, T value) {
        if (cache.size() < maxEntries) {
            cache.put(key, value);
            entryList.add(new Pair(key, value)); // when inserting, use value.getRank() in Comparator
        } else {
            Pair<K, T> entry = entryList.getLowest(); // Pair has key and value
            cache.remove(entry.getKey());
            cache.put(key, value);
            entryList.add(new Pair(key, value)); // when inserting, use value.getRank() in Comparator
        }
    }

}

//Rankable:  long getRank();

//DataSource: T get(K key);
