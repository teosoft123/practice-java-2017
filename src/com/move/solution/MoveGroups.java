package com.move.solution;

import java.io.*;
import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Brackets.
 *
 * If you need more classes, simply define them inline.
 */

class MoveGroups {
	public static void main(String[] args) {

		includeInGroup(1, 2); // 1: (1,2)
		includeInGroup(4, 5); // 2: (1,2), (4,5)
		includeInGroup(1, 4); // 

		System.out.println(totalGroups());
	}

	static Set<Set<Integer>> groups = new HashSet<Set<Integer>>();

	static void includeInGroup(Integer id1, Integer id2) {
		if (groups.isEmpty()) {
			Set<Integer> group = new HashSet<Integer>();
			group.add(id1);
			group.add(id2);
			groups.add(group);
		} else {
			for (Set<Integer> group : groups) {
				if (group.contains(id1)) {
					group.add(id2);
				} else {
					if (group.contains(id2)) {
						group.add(id1);
					}

					// Set<Integer> group2 = new HashSet<Integer>();
					// group2.add(id1);
					// group2.add(id2);
					// groups.add(group2);
					// }
					//
					//
				}
			}
		}
	}

	static int totalGroups() {
		return groups.size();
	}

}

/*
 * 1, 2 ,3 ,4 ,5
 * 
 * (1), (2) , (3), (4), (5) - tg = 5 (1,2) , (3), (4), (4)- tg = 4 (1,2,3), (4)
 * , (5) tg = 3 (1,2,4), (4,5) tg =2 tg = 1 (1,2,3,4,5)
 * 
 * iig(1,2) (1,2,3,4,5) iig(1,3) iig(4,5) iig(1,5)
 * 
 * includeInGroup(UserId id1, UserId id2) - (id1,id2) totalGroups
 */