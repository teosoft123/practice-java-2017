package com.teosoft.streams;

import java.util.stream.IntStream;

public class Streams {
	public static void main(String[] args) {
		IntStream.range(0, 10)
			.skip(3)
		    .filter(s -> { System.out.printf("filter: %d%n", s); return s < 5; } )
		    .map(s -> s*s)
		    .forEach(s -> { System.out.printf("forEach: %d%n", s);});
	}
}

