package com.teosoft.practice;

import java.util.HashMap;
import java.util.Map;

public class FibonacciDP {

    public static void main(String[] args) {
        int seed = 7;
        System.out.printf("FibonacciDP(%d) = %d%n", seed, fibdp(seed));
    }

    @SuppressWarnings("serial")
    static Map<Integer, Integer> m = new HashMap<Integer, Integer>() {
        {
            put(0, 0);
            put(1, 1);
        }
    };

    public static int fibdp(int n) {
        if (!m.containsKey(n)) {
            m.put(n, fibdp(n - 1) + fibdp(n - 2));
        }
        return m.get(n);
    }

}
