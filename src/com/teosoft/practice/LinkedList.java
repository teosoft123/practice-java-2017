package com.teosoft.practice;

public class LinkedList {

	public static class List {

		public List next;
		public int data;

		public List(int i) {
			data = i;
		}

		public static List add(List node, List tail) {
			node.next = tail;
			return node;
		}

	}

	private List head = null;

	public List getHead() {
		return head;
	}

	public LinkedList add(List l) {
		l.next = this.head;
		this.head = l;
		return this;
	}

	public static List reverse(List current) {
		List head = null;
		while (current != null) {
			List reminder = current.next;
			current.next = head;
			head = current;
			current = reminder;
		}
		return head;
	}

	public static void print(List l) {
		while (l != null) {
			System.out.printf("this: %x, data: %d%n", System.identityHashCode(l), l.data);
			l = l.next;
		}
	}

	public static void printPostOrder(List l) {
		if (l != null) {
			printPostOrder(l.next);
			System.out.printf("this: %x, data: %d%n", System.identityHashCode(l), l.data);
		}
	}

	public static interface ListGen {
		void nextItem(List l);
	}

	public static void printInOrder(List l, boolean postOrder) {
		ListGen lg = (List element) -> {
			System.out.printf("this: %x, data: %d%n", System.identityHashCode(element), element.data);
		};
		if (l != null) {
			if (postOrder) {
				printInOrder(l.next, postOrder);
				lg.nextItem(l); // in real life generator will not return the
								// entire list but perhaps create a deep copy of
								// the element,
								// just return data, etc.
			} else {
				lg.nextItem(l);
				printInOrder(l.next, postOrder);
			}
		}
	}

	public static void main(String[] args) {

		// withInstanceHead();

		List l = new List(1);
		l = List.add(new List(2), l);
		l = List.add(new List(3), l);
		l = List.add(new List(4), l);
		l = List.add(new List(5), l);
		l = List.add(new List(6), l);
		l = List.add(new List(7), l);
		l = List.add(new List(8), l);
		l = List.add(new List(9), l);

		LinkedList.print(l);
		System.out.println();
		// LinkedList.print(LinkedList.reverse(l));
		LinkedList.printInOrder(l, false);

	}

	private static void withInstanceHead() {
		// create a list
		LinkedList l = new LinkedList();
		// populate
		l.add(new List(1)).add(new List(2)).add(new List(3)).add(new List(4)).add(new List(5)).add(new List(6))
				.add(new List(7)).add(new List(8));
		// print
		LinkedList.print(l.head);
		System.out.println();
		LinkedList.print(LinkedList.reverse(l.head));
	}

}
