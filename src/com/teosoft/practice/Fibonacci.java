package com.teosoft.practice;

public class Fibonacci {

	public static void main(String[] args) {
	    for(int n = 0; n <= 7; n++) {
            System.out.printf("n: %d, fib(n): %d, sumn(n): %d%n", n, fib(n), fib2(n));
	    }
	}
	
	public static int fib(int n) {
		if(n <= 1) {
			return n;
		}
		else {
			return fib(n-1) + fib(n-2);
		}
	}
	
	public static int fib2(int n) {
        if(n <= 1) {
            return n;
        }
        int[] fib = new int[n+1];
        fib[0] = 0;
        fib[1] = 1;
                
        for(int f = 2; f <= n; f++) {
            fib[f] = fib[f-1] + fib[f-2];
        }
        return fib[n];
	}

}
