package com.teosoft.practice;

public class MergeSort {

	private int[] numbers, helper;

	public void mergeSortArray(int[] values) {
		// TODO validate input
		numbers = values;
		helper = new int[values.length];
		mergeSort(0, values.length - 1);

	}

	private void mergeSort(int low, int high) {
		if (low < high) {
			int middle = low + (high - low) / 2;
			mergeSort(low, middle);
			mergeSort(middle + 1, high);
			merge(low, middle, high);
		}
	}

	private void merge(int low, int middle, int high) {
		System.arraycopy(numbers, 0, helper, 0, helper.length);
		int i = low, j = middle + 1, k = low;
		// proceed for i..middle and j.. high, i.e. go over first and second
		// parts of array
		while (i <= middle && j <= high) {
			if (helper[i] <= helper[j]) {
				numbers[k] = helper[i]; // fill numbers[k] with the smaller one
				i++; // advance index for first part
			} else {
				numbers[k] = helper[j];
				j++; // advance index for second part
			}
			k++; // advance index for result
		}

		while (i <= middle) { // if something left in the first part, copy it to the rest
			numbers[k] = helper[i];
			k++;
			i++;
		}

	}

	public static void main(String args[]) {
		MergeSort ms = new MergeSort();
		int in[] = {1,3,2,7,6,5,-3};
		ms.mergeSortArray(in);
		for(int i=0; i<in.length; i++) {
			System.out.println(in[i]);
		}
	}
	
}
