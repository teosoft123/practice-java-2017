package com.teosoft.trees;

public class BinaryTreeMaker {

    public static void main(String[] args) {
        BinaryTreeMaker tree = new BinaryTreeMaker();

		/* Assume that inorder traversal of following tree is given
			40
			/ \
		10	   30
		/		 \
	   5		 28    */
        int[] inorder = new int[]{5, 10, 40, 30, 28};
        int len = inorder.length;
        Node mynode = tree.buildTree(inorder, 0, len - 1);

        /* Let us test the built tree by printing Inorder traversal */
        System.out.println("Inorder traversal of the constructed tree is ");
        tree.printInorder(mynode);
    }

    /* UTILITY FUNCTIONS */

    /* Recursive function to construct binary of size len from
    Inorder traversal inorder[]. Initial values of start and end
    should be 0 and len -1. */
    Node buildTree(int[] inorder, int start, int end) {
        if (start > end)
            return null;

        /* Find index of the maximum element from Binary Tree */
        int i = max(inorder, start, end);

        /* Pick the maximum value and make it root */
        Node node = new Node(inorder[i]);

		/* If this is the only element in inorder[start..end],
		then return it */
        if (start == end)
            return node;

		/* Using index in Inorder traversal, construct left and
		right subtress */
        node.left = buildTree(inorder, start, i - 1);
        node.right = buildTree(inorder, i + 1, end);

        return node;
    }

    /* Function to find index of the maximum value in arr[start...end] */
    int max(int[] arr, int strt, int end) {
        int i, max = arr[strt], maxind = strt;
        for (i = strt + 1; i <= end; i++) {
            if (arr[i] > max) {
                max = arr[i];
                maxind = i;
            }
        }
        return maxind;
    }

    /* This funtcion is here just to test buildTree() */
    void printInorder(Node node) {
        if (node == null)
            return;

        /* first recur on left child */
        printInorder(node.left);

        /* then print the data of node */
        System.out.print(node.data + " ");

        /* now recur on right child */
        printInorder(node.right);
    }
}
