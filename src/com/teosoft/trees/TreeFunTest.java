package com.teosoft.trees;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.teosoft.trees.Node.n;

public class TreeFunTest {


    private final TreeFun sut = new TreeFun();

    @Test(dataProvider = "isMirrorTestDP", enabled = true)
    public void isMirrorTest(Node a, Node b, boolean expectedResult) {
//        Assert.assertEquals(sut.TraverseBfs(a, b);, expectedResult);
    }

    @DataProvider
    public Object[][] isMirrorTestDP() {
        return new Object[][]{
                new Object[]{null, null, true},
                new Object[]{n(5), null, false},
                new Object[]{null, n(3), false},
                new Object[]{n(1), n(1), true},
                new Object[]{n(n(2), 1), n(1, n(2)), true},
                new Object[]{n(n(2), 1, n(3)), n(n(3), 1, n(2)), true},
                new Object[]{n(n(2), 1, n(3)), n(1, n(2)), false},
        };
    }

}
