package com.teosoft.trees;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import static com.teosoft.trees.Node.n;


public class TreeFun {

    public static void main(String... args) {
        Node t1 = n(n(n(n(n(4), 5), 6), 7, n(8)), 9, n(n(10), 11, n(12, n(13))));

        TreeFun tf = new TreeFun();
//        tf.TraverseBfs(t1, n -> System.out.println(n.data));

        BinaryTreeMaker binaryTreeMaker = new BinaryTreeMaker();
        int[] data = IntStream.rangeClosed(1, 5).toArray();
        Node root = binaryTreeMaker.buildTree(data, 0, data.length - 1);
        tf.TraverseBfs(root, n -> System.out.println(n.data));
    }

    public void TraverseBfs(Node n, Consumer<Node> c) {
        Queue<Node> q = new LinkedBlockingQueue<>();
        q.add(n);
        TraverseBfsImpl(q, c);
    }

    private void TraverseBfsImpl(Queue<Node> q, Consumer<Node> c) {
        while (null != q.peek()) {
            Node n = q.remove();
            if (null != n.left) {
                q.add(n.left);
            }
            if (null != n.right) {
                q.add(n.right);
            }
            c.accept(n);
        }
    }


}
