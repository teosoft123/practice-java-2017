package com.teosoft.trees;

public class Node {
    public int data;
    public Node left;
    public Node right;

    public Node(int data) {
        this.data = data;
    }

    public Node(Node left, int data, Node right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        return String.format("Node [data=%s, left=%s, right=%s]", data, left, right);
    }

    // tree creating helpers
    public static Node n(int data) {
        return new Node(data);
    }

    public static Node n(Node left, int data, Node right) {
        return new Node(left, data, right);
    }

    public static Node n(Node left, int data) {
        return new Node(left, data, null);
    }

    public static Node n(int data, Node right) {
        return new Node(null, data, right);
    }


}

