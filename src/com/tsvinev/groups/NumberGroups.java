/**
 * The positive odd numbers are sorted in ascending order as 1.3.5.7, and grouped as (1), (3,5), (7,9,11) and so on.
 * Thus, the first group is (1), the second group is (3,5), the third group is (7,9,11), etc. In general, the Kth group contains the next K elements of the sequence.
 * Given K, find the sum of the elements of the Kth group. For example, for K=3, the answer is 27:
 * 7 + 9 + 11 = 27
 * Complete the function sumOfGroup with input integer . Return the sum of the elements of the Kth group.
 * <p>
 * Oleg's note: this stream-based implementation is too slow for 50% of their test cases,
 * however my goal of the exercise was to work with streams.
 */

package com.tsvinev.groups;

import java.util.stream.LongStream;

public class NumberGroups {

    public static void main(String... args) {
        System.out.println(sumOfGroups(3));
    }

    private static long sumOfGroups(int k) {
//        LongStream.range(1, k).forEach(g -> s = s.skip(g));
        LongStream s = LongStream.iterate(1, n -> n + 2);
        for (long g = 1; g < k; g++) {
            s = s.skip(g);
        }
        return s.limit(k).reduce(0, (a, b) -> a + b);
    }

}
