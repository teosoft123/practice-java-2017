package com.tsvinev.java8;

import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.Map;
import java.util.stream.IntStream;

public class Factorial {

    private final Map<Integer, Integer> fibMAp = new HashMap<>();

    {
        fibMAp.put(2, 1);
    }

    public static void main(String... args) {
        Factorial f = new Factorial();
        System.out.println(f.factorial(10));
//        System.out.println(f.fibonacci(9));
//        System.out.println(f.sumn(5));
//        System.out.println(f.repeatingNumber(new int[]{1, 2, 3, 4, 4, 5, 6}));
    }

    public int factorial(int n) {
        return IntStream.rangeClosed(1, n).reduce(1, (a, b) -> a * b);
    }

    public int repeatingNumber(int[] nums) {
        return IntStream.of(nums).reduce(0, (a, b) -> a ^ b);
    }

    public int fibonacci(int n) {
        if (n < 2) {
            return n;
        }
        if (fibMAp.containsKey(n)) {
            return fibMAp.get(n);
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }

    }

    public IntSummaryStatistics sumn(int n) {
        return IntStream.rangeClosed(0, n).summaryStatistics();
    }
}
