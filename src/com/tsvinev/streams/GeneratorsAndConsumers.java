package com.tsvinev.streams;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class GeneratorsAndConsumers {

    public static void main(String... args) {
        IntStream.generate(new Igen()).forEach(System.out::println);
    }

    public static class Igen implements IntSupplier {

        int n = 1;

        @Override
        public int getAsInt() {
            return n++;
        }
    }

}
