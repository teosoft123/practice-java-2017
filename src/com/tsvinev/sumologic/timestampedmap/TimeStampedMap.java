package com.tsvinev.sumologic.timestampedmap;

import java.util.*;

public class TimeStampedMap {

    private Map<String, List<Pair>> map = new HashMap<>();

    public void put(String key, Integer value, Integer time) {
        Pair p = new Pair(value, time);
        List<Pair> v = map.get(key);
        if (null == v) {
            List<Pair> e = new ArrayList<>();
            e.add(p);
            map.put(key, e);
        } else {
            v.add(p);
        }
    }

    public Integer get(String key, int time) {
        Integer result = null;
        List<Pair> v = map.get(key);
        if (null == v) {
            return null;
        } else {
            int i = Collections.binarySearch(v, new Pair(0, time), Comparator.comparingInt(o -> o.t));
            if (i >= 0) {
                return v.get(i).v;
            } else {
                // if not found, returns (-(insertion point) - 1)
                // and we need an element before, so:
                return v.get(-i - 2).v;
            }
        }
    }

    class Pair {
        int v;
        int t;

        public Pair(int v, int t) {
            this.v = v;
            this.t = t;
        }

    }
}

class Solution {
    public static void main(String[] args) {
        TimeStampedMap map = new TimeStampedMap();
        map.put("Oleg", 100, 1);
        map.put("Oleg", 200, 3);
        map.put("Oleg", 150, 4);
        System.out.println(map.get("Oleg", 3));
        System.out.println(map.get("Oleg", 2));
        System.out.println(map.get("Oleg", 1));
    }
}

/*
Your previous Python 3 content is preserved below:

##
# In a hashtable you can only ask for the most recent value for a key. We are going to build a hashtable where you can # ask for a key at any time in the past. The API is:

# get(k, t) // Get the value for k at time t
# put(k, v) // Insert k, v at time()

Oleg, 100  1 <- timestamp
Oleg, 200  3
Oleg, 150  4

get(Oleg, 3) 200
get(Oleg, 2) 100
# You are given time() which you can think of as a monotonically increasing version of unix system time.

value = (v, t)

value = list<(v, t)>

key -> list (v, t)

put -> O(1)
get -> O(n)

get -> O(log(n))

map = {}

def





 */