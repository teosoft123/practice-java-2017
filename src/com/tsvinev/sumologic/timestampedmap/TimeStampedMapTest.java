package com.tsvinev.sumologic.timestampedmap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.tsvinev.sumologic.timestampedmap.TimeStampedMapTest.PairTime.p;
import static org.testng.Assert.assertEquals;

public class TimeStampedMapTest {

    @Test(dataProvider = "timeseries")
    public void timeSeriesMapTest(PairTime[] kvt, PairTime exp) {
        TimeStampedMap sut = new TimeStampedMap();

        for (PairTime pvt : kvt) {
            sut.put(pvt.k, pvt.v, pvt.t);
        }
        int i = sut.get(exp.k, exp.t);
        assertEquals(i, exp.v);
    }

    @DataProvider
    public Object[][] timeseries() {
        return new Object[][]{
                new Object[]{
                        new PairTime[]{
                                p("Oleg", 100, 1),
                                p("Oleg", 200, 3),
                                p("Oleg", 150, 4),
                        },
                        p("Oleg", 100, 2)},
        };
    }

    static class PairTime {
        String k;
        int v;
        int t;

        public PairTime(String k, int v, int t) {
            this.k = k;
            this.v = v;
            this.t = t;
        }

        public static PairTime p(String k, int v, int t) {
            return new PairTime(k, v, t);
        }
    }

}