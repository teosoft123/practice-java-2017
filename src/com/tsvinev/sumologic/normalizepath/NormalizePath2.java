package com.tsvinev.sumologic.normalizepath;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class NormalizePath2 extends NormalizePath {

    public String normalize(String s) {
        String[] components = s.split("/");

        if (dontProcess(s, components)) {
            return s;
        }

        ArrayList<String> outList = new ArrayList();

        for (int i = 0; i < components.length; i++) {
            String c = components[i];
            switch (c) {
                case ".":
                    continue;
                case "":    // the result of "//"
                    continue;
                case "..":  // most interesting
                    if (!outList.isEmpty()) {
                        outList.remove(outList.size() - 1);
                    }
                    continue;
                default:
                    outList.add(c);
            }
        }

//        StringBuilder sb = new StringBuilder("/");
//        for (Iterator<String> i = outList.iterator(); i.hasNext(); ) {
//            sb.append(i.next());
//            if (i.hasNext()) {
//                sb.append("/");
//            }
//        }
//        return sb.toString();

        return outList.stream().collect(Collectors.joining("/", "/", ""));
    }

}
