package com.tsvinev.sumologic.normalizepath;

public abstract class NormalizePath implements INormalizePath {
    boolean dontProcess(String s, String[] components) {
        return null == s || s.isEmpty() || components.length < 2;
    }


}
