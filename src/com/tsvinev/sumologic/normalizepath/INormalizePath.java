package com.tsvinev.sumologic.normalizepath;

public interface INormalizePath {
    String normalize(String s);

}
