package com.tsvinev.sumologic.normalizepath;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class INormalizePathTest {

    INormalizePath sut1 = new NormalizePath1();
    INormalizePath sut2 = new NormalizePath2();

    @Test(dataProvider = "pathData")
    public void testNormalizeForward(String data, String expected) {
//        assertEquals(sut1.normalize(data), expected);
        assertEquals(sut2.normalize(data), expected);
    }

    @DataProvider
    public Object[][] pathData() {
        return new Object[][]{
                new Object[]{"/Users/../etc/./ssh/", "/etc/ssh"},
                new Object[]{"/home/", "/home"},
                new Object[]{"/a/./b/../../c/", "/c"},
                new Object[]{"/a/..", "/"},
                new Object[]{"/a/../", "/"},
                new Object[]{"/../../../../../a", "/a"},
                new Object[]{"/a/./b/./c/./d/", "/a/b/c/d"},
                new Object[]{"/a/../.././../../.", "/"},
                new Object[]{"/a//b//c//////d", "/a/b/c/d"},


        };
    }

}
