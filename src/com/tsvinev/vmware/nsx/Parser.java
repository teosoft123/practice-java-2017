package com.tsvinev.vmware.nsx;/* Implement a small parser (similar to C++ preprocessor) to parse #ifdef, #ifndef, #else, #endif
 * keywords. Input will be an array of strings and a set of symbols that are defined to
 * be true. Example -
 * Input: strings = [ "AAA", "#ifdef x", "BBB", "CC", "#else", "DDD", "#endif", "EEEE" ], defined = ["x"]
 * Output if 'x' is defined = [ "AAA", "BBB", "CC", "EEEE" ]
 * Output if 'x' is not defined = [ "AAA", "DDD", "EEEE" ]
 */


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Parser {

    static List<String> input = Arrays.asList("AAA", "#ifdef x", "BBB", "CC", "#else", "DDD", "#endif", "EEEE");
    static Set<String> defined_vars = new HashSet();

    static {
        defined_vars.addAll(Arrays.asList("x"));
    }

    public static void main(String... args) {

        boolean toPrint = true;

        for (String s : input) {
            if ('#' == s.charAt(0)) {
                String[] cond = s.split(" ");
                switch (cond[0]) {
                    case "#ifdef":
                        toPrint = defined_vars.contains(cond[1]);
                        break;
                    case "#else":
                        toPrint = !toPrint;
                        break;
                    case "#ifndef":
                        toPrint = !defined_vars.contains(cond[1]);
                        break;
                    case "#endif":
                        toPrint = true;
                        break;
                    default:
                        // something else - error?
                }
            } else {
                if(toPrint) {
                    System.out.println(s);
                }
            }

        }

    }

}