package twilio;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.internal.collections.Ints;

import static org.testng.Assert.assertEquals;

public class SortArrayFreqValueTest {

    SortArrayFreqValue sut1 = new SortFreqValue2();
    SortArrayFreqValue sut2 = new SortFreqValueStream();

    @Test(dataProvider = "lists")
    public void mustPass(SortArrayFreqValue sut, int[] list, int[] sorted) {
        assertEquals(sut.sortFreqValue(Ints.asList(list)), Ints.asList(sorted));
    }

    @DataProvider
    public Object[][] lists() {
        return new Object[][]{
                new Object[]{sut1, new int[]{3, 1, 2, 2, 4}, new int[]{1, 3, 4, 2, 2}},
                new Object[]{sut2, new int[]{3, 1, 2, 2, 4}, new int[]{1, 3, 4, 2, 2}},
        };
    }

}