package twilio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import static java.util.stream.Collectors.toConcurrentMap;

public class SortFreqValue2 implements SortArrayFreqValue {

    public static void main(String... args) {
        List<Integer> list = new SortFreqValue2().sortFreqValue(Arrays.asList(3, 1, 2, 2, 4));
        list.forEach(System.out::println);
    }

    @Override
    public List<Integer> sortFreqValue(List<Integer> list) {
        // build frequency histogram first
        ConcurrentMap<Integer, Integer> freqHist = list.parallelStream().collect(toConcurrentMap(i -> i, i -> 1, Integer::sum));
        Comparator<Integer> chainedComparator = (i1, i2) -> {
            int freqComp = freqHist.get(i1).intValue() - freqHist.get(i2).intValue();
            if (freqComp == 0) {
                return i1.compareTo(i2);
            } else {
                return freqComp;
            }
        };
        List<Integer> out = new ArrayList<>();
        out.addAll(list);
        out.sort(chainedComparator);
        return out;
    }
}

// Java 7 combined comparators idea isn't as nice as Java 7

//        Comparator<? super Integer> freqComparator = new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return freqHist.get(o1).intValue() - freqHist.get(o2).intValue();
//            }
//        };
//        Comparator<? super Integer> integerComparator = new Comparator<Integer>() {
//
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return o1.compareTo(o2);
//            }
//        };
//
//        Comparator<Integer> combined = Comparator.comparing()


