package twilio;

import java.util.List;

/***
 * Problem: given an array [3, 1, 2, 2, 4]
 * output array sorted by frequency first, then by value
 * [[3,1,4][2,2]] - intermediate result
 * final result... ? [1, 3, 4, 2, 2]
 */
public interface SortArrayFreqValue {

    /**
     * @param list
     * @return list ordered by frequency first and value second
     */
    List<Integer> sortFreqValue(List<Integer> list);

}
