package twilio;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class SortArrayChainedComparators implements SortArrayFreqValue {

    public static void main(String... args) {
        List<Integer> list = Arrays.asList(3, 1, 2, 2, 4);
        new SortFreqValueStream().sortFreqValue(list)
                .stream().forEach(System.out::println);
    }

    @Override
    public List<Integer> sortFreqValue(List<Integer> list) {

        Map<Integer, Integer> frequencyHistogram = list.stream().collect(toMap(i -> i, i -> 1, Integer::sum));
//        ConcurrentMap<Integer, Integer> frequencyHistogram = list.parallelStream().collect(toConcurrentMap(i -> i, i -> 1, Integer::sum));
        return list.stream().sorted(Comparator.<Integer>comparingInt(frequencyHistogram::get)
                .thenComparing(Comparator.naturalOrder())).collect(Collectors.toList());
    }

}



