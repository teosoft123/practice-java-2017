package twilio;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import static java.util.stream.Collectors.*;

/**
 * See problem description in interface
 */
public class SortArrayFreqValueStreams implements SortArrayFreqValue {

    public static void main(String... args) {
        List<Integer> list = Arrays.asList(3, 1, 2, 2, 4);
        SortArrayFreqValue o = new SortArrayFreqValueStreams();
        o.sortFreqValue(list);
    }

    @Override
    public List<Integer> sortFreqValue(List<Integer> list) {

//        Map<Integer, Integer> collect =
//                IntStream.of(ints).boxed().collect(Collectors.toConcurrentMap(i->i, i->1, Integer::sum));

        ConcurrentMap<Integer, Integer> orderedByFrequency = list.parallelStream().collect(toConcurrentMap(i -> i, i -> 1, Integer::sum));

        Set<Map.Entry<Integer, Integer>> es = orderedByFrequency.entrySet();
//        es.stream().forEach(e -> System.out.printf("value: %d, count: %d%n", e.getKey(), e.getValue()));
        // we already have pairs of value:count, so now we need to
        // output values for which count is 1, first, sorted,
        // then those for which count is 2, sorted
        // and so on
        // SHOULD'VE went with GroupBy???


        Map<Integer, Long> gb = es.stream().collect(groupingBy(e -> e.getValue(), counting()));
        gb.forEach((x, y) -> System.out.printf("value: %d, count: %d%n", x.intValue(), y.intValue()));


//        Map<Integer, List<Integer>> m = list.stream().collect(groupingBy(Integer::intValue));
//        m.entrySet().forEach(e->System.out.printf("value: %d, count: %d%n", e.getKey(), e.getValue()));

//        Stack<Map.Entry<Integer, Integer>> stack = new Stack<>();
//        for (Map.Entry<Integer, Integer> e : es) {
//
//            System.out.printf("value: %d, count: %d%n", e.getKey(), e.getValue());
//        }

//        int[] input = new int[]{3, 1, 2, 2, 4};
//
//        Map<String, Integer> collect =
//                IntStream.of(input).collect(groupingBy(Function.identity(), summingInt(e -> 1)));


//        orderedByFrequency.entrySet().stream().spliterator()

//                forEach(x,y -> System.out.printf("x: %d, y: %d", x, y));
        return null;
    }


}
