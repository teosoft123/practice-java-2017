package twilio;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toConcurrentMap;

public class SortFreqValueStream implements SortArrayFreqValue {
    public static void main(String... args) {
        List<Integer> list = Arrays.asList(3, 1, 2, 2, 4);
        new SortFreqValueStream().sortFreqValue(list)
                .stream().forEach(System.out::println);
    }

    @Override
    public List<Integer> sortFreqValue(List<Integer> list) {

        ConcurrentMap<Integer, Integer> freqHist = list.parallelStream().collect(toConcurrentMap(i -> i, i -> 1, Integer::sum));
        // Driving it by a set of map entries loses data because of duplicates in input.
//        return freqHist.entrySet().stream().sorted(
//                Map.Entry.<Integer, Integer>comparingByValue()
//                        .thenComparing(Map.Entry.comparingByKey()))
//                .map(Map.Entry::getKey).collect(Collectors.toList());

        /**
         * So the key was to write a chained comparator
         */
        Comparator<Integer> chainedComparator = (i1, i2) -> {
            int freqComp = freqHist.get(i1).intValue() - freqHist.get(i2).intValue();
            if (freqComp == 0) {
                return i1.compareTo(i2);
            } else {
                return freqComp;
            }
        };

        return list.stream().sorted(chainedComparator).collect(Collectors.toList());
    }

}



