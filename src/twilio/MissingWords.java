package twilio;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

class Result {

    /*
     * Complete the 'missingWords' function below.
     *
     * The function is expected to return a STRING_ARRAY.
     * !!! BUT THE signature is List<String> missingWords(String s, String t)
     * The function accepts following parameters:
     *  1. STRING s
     *  2. STRING t
     */

    public static List<String> missingWords(String s, String t) {
        LinkedHashSet<String> ls1 = new LinkedHashSet();
        ls1.addAll(Arrays.asList(s.split("\\s+")));
        LinkedHashSet<String> ls2 = new LinkedHashSet();
        ls2.addAll(Arrays.asList(t.split("\\s+")));
        ls1.removeAll(ls2);
        return ls1.stream().collect(Collectors.toList());
    }

}

public class MissingWords {

    public static void main(String... args) {
        String s1 = "I am using HackerRank to improve programming";
        String s2 = "am HackerRank to improve";
        List<String> s = Result.missingWords(s1, s2);
        s.stream().forEach(System.out::println);
    }
}

