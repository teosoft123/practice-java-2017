package tallestcandles;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.TreeMap;

public class Solution {

	static {
		String input = "4 3 2 1 3";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	static int birthdayCakeCandles(int n, int[] ar) {
		NavigableMap<Integer, Integer> cake = new TreeMap<>();
		for (int i = 0; i < n; i++) {
			if (!cake.containsKey(ar[i])) {
				cake.put(ar[i], 1);
			} else {
				Integer count = cake.get(ar[i]);
				cake.put(ar[i], count + 1);
			}
		}
		Entry<Integer, Integer> x = cake.lastEntry();
		return x.getValue();
	}

	public static void main(String[] args) {

		try (Scanner in = new Scanner(System.in)) {
			int n = in.nextInt();
			int[] ar = new int[n];
			for (int ar_i = 0; ar_i < n; ar_i++) {
				ar[ar_i] = in.nextInt();
			}
			int result = birthdayCakeCandles(n, ar);
			System.out.println(result);
		}
	}
}
