package hackerrank;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class Solution {

	static {
		String input = "3 11 2 4 4 5 6 10 8 -12";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	private static boolean isOnPrimaryDiagonal(int row, int col) {
		return row == col;
	}

	private static boolean isOnSecondaryDiagonal(int size, int row, int col) {
		return size - 1 == row + col; // better to remember and understand
	}

	private static int diagonalDifference(int size, Scanner in) {
		int primarySum = 0, secondarySum = 0;

		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				int n = in.nextInt();
				if (isOnPrimaryDiagonal(row, col)) {
					primarySum += n;
				}
				if (isOnSecondaryDiagonal(size, row, col)) {
					secondarySum += n;
				}
			}
		}
		System.out.printf("primary: %d, secondary: %d%n", primarySum, secondarySum);
		return Math.abs(primarySum - secondarySum);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int size = in.nextInt();
			System.out.println(diagonalDifference(size, in));
		}
	}
}
