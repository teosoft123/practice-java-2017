package hackerrank;

import java.util.Scanner;
import java.util.stream.Stream;

public class AliceVsBob {

	static void score(int alice, int bob, int[] scores) {
		if (alice != bob) {
			if (alice > bob) {
				scores[0]++;
			} else {
				scores[1]++;
			}
		}
	}

	static int[] solve(int a0, int a1, int a2, int b0, int b1, int b2) {
		// Complete this function
		int[] result = new int[2];
//		Stream.of(a0, b0, a1, b1, a2, b2)
//			.reduce((a, b) -> { if(a != b) if(a > b) result[0]++; else result[1]++; })
//			.forEach(s -> System.out.println("forEach: " + s));
//		 score(a0, b0, result);
//		 score(a1, b1, result);
//		 score(a2, b2, result);
		return result;
	}

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		int a0 = 5; // in.nextInt();
		int a1 = 6; // in.nextInt();
		int a2 = 7; // in.nextInt();
		int b0 = 3; // in.nextInt();
		int b1 = 6; // in.nextInt();
		int b2 = 10; // in.nextInt();
		int[] result = solve(a0, a1, a2, b0, b1, b2);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
		}
		System.out.println("");

	}
}
