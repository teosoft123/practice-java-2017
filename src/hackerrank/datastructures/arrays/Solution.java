package hackerrank.datastructures.arrays;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class Solution {

	static {
		String input = "4 1 4 3 2";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int size = in.nextInt();
			int[] ar = new int[size];
			for (int i = 0; i < size; i++) {
				ar[i] = in.nextInt();
			}
			for (int i = size - 1; i >= 0; i--) {
				System.out.printf("%d ", ar[i]);
			}
			System.out.println();
		}
	}

}