package hackerrank.datastructures.brackets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class Brackets {

	static {
		
//		String input = "6\n}][}}(}][))]\n[](){()}\n()\n({}([][]))[]()\n{)[](}]}]}))}(())(\n([[)\n";

		InputStream myIn;
		try {
			myIn = new ByteArrayInputStream(Files.readAllBytes(Paths.get("/Users/oleg/projects/practice-java-2017/src/hackerrank/datastructures/brackets/input09.txt")));
			System.setIn(myIn);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static Set<Character> opening = new HashSet<>(Arrays.asList('{', '[', '('));
	static Set<Character> closing = new HashSet<>(Arrays.asList('}', ']', ')'));
	static Map<Character, Character> pairs = new HashMap<>();

	static {
		pairs.put('{', '}');
		pairs.put('[', ']');
		pairs.put('(', ')');
	}

	private static void isBalanced(String s) {
		Stack<Character> stack = new Stack<>();

		s.chars().mapToObj(i -> (char) i).forEach(i -> {
			if (opening.contains(i)) {
				stack.push(i);
			} else {
				if (!stack.isEmpty()) {
					Character c = stack.pop();
					if (closing.contains(i) && pairs.get(c).equals(i)) {
					} else {
						stack.push(c);
					}
				}
			}
		});

		if (stack.isEmpty()) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int t = in.nextInt();

			for (int a0 = 0; a0 < t; a0++) {
				String s = in.next();
				if(closing.contains(s.charAt(0))) {
					System.out.println("NO");
				}
				else {
					isBalanced(s);
				}
			}
		}
	}

}
