package hackerrank.datastructures.arrays2;

import java.io.*;
import java.util.*;

public class Solution {

	static {
		String input = "5 4\n1 2 3 4 5";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int size = in.nextInt();
			int rotateBy = in.nextInt();
			int[] array = new int[size];
			int[] array2 = new int[size];
			for(int i=0; i < array.length; i++) {
				array2[i] = array[i] = in.nextInt();
			}
			rotateArrayBy(rotateBy, array, array2);
			for(int i: array) {
				System.out.printf("%d ", i);
			}
			System.out.println();
		}
	}

	private static void rotateArrayBy(int rotateBy, int[] array, int[] array2) {
		int size = array.length;
		for(int src = 0, dst = rotateBy; src < rotateBy; src++, dst++) {
			array[dst] = array[src];
		}
		for(int src = rotateBy, dst = 0; dst < size - rotateBy; src++, dst++) {
			array[dst] = array2[src];
		}
	}
	
}