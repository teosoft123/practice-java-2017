package hackerrank.datastructures.stacks;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class Solution {

	static {
		String input = "10 1 97 2 1 20 2 1 26 1 20 2 3 1 91 3";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	static int max = Integer.MIN_VALUE;

	static public class Node {
		public int data;
		public Node next;

		public Node(int data, Node next) {
			this.data = data;
			this.next = next;
		}
	}

	static Node head = null;

	static void push(int e) {
		head = new Node(e, head); // use linked list to represent stack
		System.out.printf("1 %d%n", e);
		if (e > max) {
			max = e;
		}
	}

	static void delete() {
		System.out.printf("2%n");
		if (null != head) {
			head = head.next;
			// find new max
			max = Integer.MIN_VALUE;
			for (Node h = head; h != null; h = h.next) {
				if (h.data > max) {
					max = h.data;
				}
			}
		}
	}

	static void max() {
		System.out.printf("3 %d%n", max);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			@SuppressWarnings("unused")
			int nQueries = in.nextInt();
			while (in.hasNext()) {
				int query = in.nextInt();
				switch (query) {
				case 1:
					push(in.nextInt());
					break;
				case 2:
					delete();
					break;
				case 3:
					max();
					break;
				default:
					break;
				}
			}
		}
	}
}