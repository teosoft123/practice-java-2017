package atlassian;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class EnclodeMod7 {

	static {
		String input = "1 7";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	public static void main(String[] args) {

		char[] b7 = { '0', 'a', 't', 'l', 's', 'i', 'n' };
		try (Scanner in = new Scanner(System.in)) {
			int t = in.nextInt();

			int i = t;
			StringBuffer sb = new StringBuffer();
			while (i > 0) {
				int m = i % 7;
				sb.append(b7[m]);
				i = i / 7;
			}

			System.out.println(sb.reverse());
		}
	}
}
