package atlassian;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class LookAndSay {

	static {
		String input = "111221"; // 312221
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			String s = in.next();
			lookAndSay(s);
		}
	}

	private static void lookAndSay(String s) {

		int count = 0;
		Character c = s.charAt(0);
		for (int i = 0; i < s.length(); i++) {
			if(c.equals(s.charAt(i))) {
				count++;
				continue;
			}
			System.out.printf("%d%c", count, c);
			c = s.charAt(i);
			count = 1;
		}
		System.out.printf("%d%c", count, c);
	}

}
