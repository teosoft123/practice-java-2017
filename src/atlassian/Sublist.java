package atlassian;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Sublist {

	static {
		String input = "3 1 2 3 2 2 3";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			System.out.println(sublist(readList(in), readList(in)));
		}
		catch (InputMismatchException e) {
			System.out.printf("%d%n", -1);
		}
	}

	private static List<Integer> readList(Scanner in) {
		int len = in.nextInt();
		List<Integer> list = new ArrayList<>(len);
		for (int i = 0; i < len; i++) {
			list.add(in.nextInt());
		}
		System.out.println(Arrays.toString(list.toArray()));			
		return list;
	}

	private static int sublist(List<Integer> source, List<Integer> target) {
		return Collections.indexOfSubList(source, target);
	}

}
