package net.tsvinev.combinatorics;

import java.util.HashSet;
import java.util.Set;

/**
 * To find all subsets of a given set, represent each element as a bit, 1 value
 * meaning element is present, 0 value means element is absent.
 *
 * All combinations of these bits will represent all subsets of a set
 *
 * Every combination of these bits, viewed as a binary number, is every natural
 * number from zero (empty set) to all bits set to 1, all elements present.
 *
 * To enumerate every subset, iterate from 0 to 2^N, where N is a number of
 * element in a set.
 *
 * The only challenge is to find an efficient data structure to map from bits of
 * a binary number to elements of the set.
 *
 * Let's limit it to integer, i.e. 2^31 - 1 That's a lot already.
 */
public class AllSubsets {

    public static Set<Set<?>> getAllSubsets(Set<?> set) {
        Set<Set<?>> result = new HashSet<>();
        int n = set.size();
        int maxValue = (1 << n) - 1;
        Object[] setAsArray = set.toArray();
        for (int i = 0; i <= maxValue; i++) {
            Set<Object> subset = getPresentElements(i, setAsArray);
            result.add(subset);
        }
        return result;
    }

    /**
     * To map bits positions onto original set of objects, we need the original
     * set to preserve the order of elements between invocations of get()
     * methods. It seems to be best to dump elements into array and index this
     * array by bit number. Even better if the original set comes as a set. But
     * really we're talking about max 32 elements here for int, so it doesn't
     * seem to be a problem.
     *
     * On the other hand, if we want repeating elements (i.e bags not sets) then
     * taking an array as input is only natural.
     */
    private static Set<Object> getPresentElements(int i, Object[] set) {
        Set<Object> result = new HashSet<>();
        while (i > 0) {
            int lowestOneBit = Integer.lowestOneBit(i);
            int pos = Integer.numberOfTrailingZeros(lowestOneBit);
            result.add(set[pos]);
            i &= ~lowestOneBit; // set the lowest bit to zero to get the next
                                // one
        }
        return result;
    }

    public static void main(String[] args) {
        Set<Character> set = new HashSet<>();
        String chars1 = "abcdefghijklmnop";
        for(int i=0; i < chars1.length(); i++) {
            set.add(chars1.charAt(i));
        }
        long ts = System.currentTimeMillis();
        Set<Set<?>> allSubsets = getAllSubsets(set);
        long duration = System.currentTimeMillis() - ts;
        allSubsets.forEach(System.out::println);
        System.out.printf("Number of subsets: %d%n", allSubsets.size());
        System.out.printf("Enumerating all subsets took %.4f seconds", duration/1000.0);
    }

}
