package net.teosoft.dp.string.perm;

import java.util.HashSet;
import java.util.Set;

public class CharPerm {

/* Heap Algorithm:
procedure generate(n : integer, A : array of any):
        if n = 1 then
              output(A)
        else
            for i := 0; i < n - 1; i += 1 do
                generate(n - 1, A)
                if n is even then
                    swap(A[i], A[n-1])
                else
                    swap(A[0], A[n-1])
                end if
            end for
            generate(n - 1, A)
        end if
*/

    private static void generate(int n, char[] A, Set<String> acc) {
        if(n == 1) {
            acc.add(String.valueOf(A));
        }
        else {
            for(int i = 0; i < n - 1; i++) {
                generate(n-1, A, acc);
                if(n % 2 == 0) {
                    char t = A[i];
                    A[i] = A[n-1];
                    A[n-1] = t;
                }
                else {
                    char t = A[0];
                    A[0] = A[n-1];
                    A[n-1] = t;
                }
            }
            generate(n-1, A, acc);
        }
    }

    public static Set<String> generate(char[] items) {
        Set<String> acc = new HashSet<>();
        generate(items.length, items, acc);
        return acc;
    }

    public static void main(String[] args) {
        Set<String> p = generate("abc".toCharArray());
        System.out.printf("%d permutations%n", p.size());
        p.forEach(System.out::println);
    }

}
