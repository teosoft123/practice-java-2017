package net.teosoft.dp.string.perm;

import java.util.HashSet;
import java.util.Set;

public class StringPerm {

    public static Set<String> perm(String s) {
        if(null == s) {
            return null;
        }
        Set<String> acc = new HashSet<>();
        if(s.length() == 0) {
            acc.add("");
            return acc;
        }
        Character c = s.charAt(0);
        String tail = s.substring(1);
        Set<String> words = perm(tail);
        for(String word: words) {
            for(int i = 0; i <= word.length(); i++) {
                String p = insertCharAt(word, c, i);
                acc.add(p);
            }
        }
        return acc;
    }

    private static String insertCharAt(String word, Character c, int i) {
        String head = word.substring(0, i);
        String tail = word.substring(i);
        return head + c + tail;
    }

    public static void main(String[] args) {
        Set<String> p = perm("abc");
        System.out.printf("%d permutations%n", p.size());
        p.forEach(System.out::println);
    }

}
