package staircacse;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            int size = 6; // in.nextInt();
            for (int line = 1; line <= size; line++) {
                for (int pos = 0; pos < size; pos++) {
                    if (pos < size - line) {
                        System.out.printf(" ");
                    } else {
                        System.out.printf("#");
                    }
                }
                System.out.println();
            }
        }
    }
}
