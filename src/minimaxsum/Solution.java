package minimaxsum;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class Solution {
	
	static {
		String input = "1 2 3 4 5";
		InputStream myIn = new ByteArrayInputStream(input.getBytes());
		System.setIn(myIn);
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			long sum5 = 0;
			long minSum = Long.MAX_VALUE;
			long maxSum = Long.MIN_VALUE;
			long[] arr = new long[5];
			for (int arr_i = 0; arr_i < 5; arr_i++) {
				long nextLong = in.nextLong();
				sum5 += nextLong;
				arr[arr_i] = nextLong;
			}
			for (int i = 0; i < 5; i++) {
				long sum4 = sum5 - arr[i];
				if (minSum > sum4) {
					minSum = sum4;
				}
				if (maxSum < sum4) {
					maxSum = sum4;
				}
			}
			System.out.printf("%d %d%n", minSum, maxSum);
		}
	}
}
